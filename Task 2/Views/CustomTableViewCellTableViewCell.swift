//
//  CustomTableViewCell.swift
//  Task 2
//
//  Created by Sergey Parfentchik on 13.02.22.
//

import UIKit

class CustomTableViewCell: UITableViewCell {
    
    //MARK: - Public Properties
    
    let content: Content
    
    //MARK: - Private Properties
    
    private lazy var imgView = CustomView.init()
    private lazy var title = UILabel()
    private lazy var descript = UILabel()
    
    //MARK: - Initialize

    init(content: Content) {
        self.content = content
        super.init(style: .default, reuseIdentifier: Const.TableCell.id)
        initialize()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        settingImage()
        settingTitle()
        settingDescript()
        addingSubviews()
        notTranslatesAutoresizingMaskIntoConstraints()
        settingConstraints()
    }
    //MARK: - Public Methods
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        self.contentView.backgroundColor = .clear
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        self.contentView.backgroundColor = highlighted ? Const.TableCell.highlightedColor : .clear
    }
    
    //MARK: - Private Methods
    
    private func settingImage() {
        imgView.image.image = content.image
    }
    
    private func settingTitle(){
        title.text = content.title
        title.textColor = Const.TableCell.colorTitle
    }
    
    private func settingDescript(){
        descript.textColor = Const.TableCell.colorDescription
        descript.text = content.description
        descript.font = UIFont.systemFont(ofSize: Const.Font.sizeDescriptionCell)
    }
    
    private func addingSubviews() {
        contentView.addSubview(imgView)
        contentView.addSubview(title)
        contentView.addSubview(descript)
    }
    
    private func notTranslatesAutoresizingMaskIntoConstraints(){
        imgView.translatesAutoresizingMaskIntoConstraints = false
        title.translatesAutoresizingMaskIntoConstraints = false
        descript.translatesAutoresizingMaskIntoConstraints = false
    }
    
    private func settingConstraints(){
        NSLayoutConstraint.activate([
            imgView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Const.TableCell.imageLeadingOffset),
            imgView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            
            title.leadingAnchor.constraint(equalTo: imgView.trailingAnchor, constant: Const.TableCell.titleLeadingOffset),
            title.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Const.TableCell.titleTopOffset),
            
            descript.leadingAnchor.constraint(equalTo: title.leadingAnchor),
            descript.topAnchor.constraint(equalTo: title.bottomAnchor, constant: Const.TableCell.descriptionTopOffset)
        ])
    }
}
