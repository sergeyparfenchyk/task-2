//
//  Constant.swift
//  Task 2
//
//  Created by Sergey Parfentchik on 13.02.22.
//

import UIKit

struct Const {
    struct Content {
        static let imageName = "Image-"
        static let title = "Title"
        static let description = "Description "
        static let numberOfCells = 100
    }
    
    struct MainViewController {
        static let backgroundColor = UIColor.systemBackground
    }
    
    struct TableCell {
        static let id = "CellId"
        static let height = 60.0
        static let titleDeleteButton = "Delete"
        static let colorTitle = UIColor.label
        static let colorDescription = UIColor.systemGray2
        static let imageLeadingOffset = 20.0
        static let titleLeadingOffset = 10.0
        static let titleTopOffset = 10.0
        static let descriptionTopOffset = 3.0
        static let highlightedColor = UIColor.systemGray6
    }
    
    struct Image {
        static let size: CGFloat = 40.0
        static let backgroundColor = UIColor.systemGray4
        static let alpha = 0.5
    }
    
    struct Font {
        static let sizeDescriptionCell = 14.0
        static let sizeTitle = 30.0
        static let sizeBackButton = 18.0
    }
    
    struct EditButton {
        static let height = 50.0
        static let titleEdit = "Edit"
        static let titleSave = "Save"
        static let backgroundColor = UIColor.systemBackground
        static let titleColor = UIColor.systemBlue
        static let shadowColor = UIColor.systemGray4.cgColor
        static let shadowOpacity: Float = 5.0
    }
    
    struct Detail {
        static let zoomImage = 2.0
        static let nameBackImage = "chevron.backward"
        static let titleBackButton = "Back"
        static let spacingStackView = 10.0
        static let descriptionCornerRadius = 10.0
        static let imageTopOffset = 30.0
        static let stackTopOffset = 20.0
        static let stackLeadingOffset = 20.0
        static let stackTrailing = 20.0
        static let backgroundColor = UIColor.systemBackground
    }
}
