//
//  MainViewController.swift
//  Task 2
//
//  Created by Sergey Parfentchik on 12.02.22.
//

import UIKit

class MainViewController: UIViewController {
    
    //MARK: - Private Properties
    
    private lazy var tableView = UITableView()
    private lazy var editButton = UIButton()
    private lazy var contents = createHundredElements()
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Const.MainViewController.backgroundColor
        
        addingSubviews()
        notTranslatesAutoresizingMaskIntoConstraints()
        settingTableView()
        settingConstraints()
        settingEditButton()
    }
    
    //MARK: - Private Methods
    
    private func addingSubviews() {
        view.addSubview(tableView)
        view.addSubview(editButton)
    }
    
    private func notTranslatesAutoresizingMaskIntoConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        editButton.translatesAutoresizingMaskIntoConstraints = false
    }
    
    private func settingTableView() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    private func settingConstraints() {
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: editButton.topAnchor),
            
            editButton.heightAnchor.constraint(equalToConstant: Const.EditButton.height),
            editButton.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            editButton.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            editButton.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
    }
    
    private func settingEditButton() {
        editButton.backgroundColor = Const.EditButton.backgroundColor
        editButton.setTitleColor(Const.EditButton.titleColor, for: .normal)
        editButton.setTitle(Const.EditButton.titleEdit, for: .normal)
        editButton.layer.shadowColor = Const.EditButton.shadowColor
        editButton.layer.shadowOpacity = Const.EditButton.shadowOpacity
        editButton.addTarget(self, action: #selector(touchUpInsideEditButton), for: .touchUpInside)
        editButton.contentVerticalAlignment = .top
    }
    
    private func settingInfoViewControler(indexRow: Int) -> UINavigationController {
        let infoViewController = DetailViewController()
        let infoNavigationController = UINavigationController(rootViewController: infoViewController)
        infoViewController.content = contents[indexRow]
        infoNavigationController.modalPresentationStyle = .fullScreen
        infoNavigationController.modalTransitionStyle = .flipHorizontal
        return infoNavigationController
    }
    
    @objc private func touchUpInsideEditButton(button: UIButton) {
        if tableView.isEditing {
            button.setTitle(Const.EditButton.titleEdit, for: .normal)
            tableView.isEditing = false
        } else {
            button.setTitle(Const.EditButton.titleSave, for: .normal)
            tableView.isEditing = true
        }
    }
}

// MARK: - UITableViewDelegate

extension MainViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat { Const.TableCell.height}
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = UIContextualAction(style: .destructive, title: Const.TableCell.titleDeleteButton) { _, _, complete in
            self.contents.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .top)
            complete(true)
        }
        let configuration = UISwipeActionsConfiguration(actions: [delete])
        configuration.performsFirstActionWithFullSwipe = true
        return configuration
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let infoViewController = settingInfoViewControler(indexRow: indexPath.row)
        self.present(infoViewController, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
}

// MARK: - UITableViewDataSource

extension MainViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = CustomTableViewCell(content: contents[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let content = contents.remove(at: sourceIndexPath.row)
        contents.insert(content, at: destinationIndexPath.row)
    }
}

