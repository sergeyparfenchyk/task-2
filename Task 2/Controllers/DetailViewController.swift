//
//  DetailViewController.swift
//  Task 2
//
//  Created by Sergey Parfentchik on 13.02.22.
//

import UIKit

class DetailViewController: UIViewController {
    
    //MARK: - Public Properties
    
    var content: Content?
    
    //MARK: - Private Properties
    
    private lazy var imageView = CustomView(imageZoom: Const.Detail.zoomImage)
    private lazy var titleLabel = UILabel()
    private lazy var descriptionLabel = UILabel()
    private lazy var stackView = UIStackView()
    private lazy var backImage = UIImage.init(systemName: Const.Detail.nameBackImage)
    private lazy var backButton = UIButton(type: .system)
    private lazy var barItem = UIBarButtonItem(customView: backButton)
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Const.Detail.backgroundColor
        
        settingImageView()
        settingStackView()
        settingTitleLabel()
        settingDescriptionLabel()
        settingBackButton()
        addingSubviews()
        notTranslatesAutoresizingMaskIntoConstraints()
        settingConstraints()
    }
    
    //MARK: - Private Methods
    
    func settingBackButton(){
        backButton.setImage(backImage, for: .normal)
        backButton.setTitle(Const.Detail.titleBackButton, for: .normal)
        backButton.titleLabel?.font = UIFont.systemFont(ofSize: Const.Font.sizeBackButton)
        backButton.addTarget(self, action: #selector(clovesViewController), for: .touchUpInside)
        navigationController?.navigationBar.topItem?.leftBarButtonItem = barItem
    }
    
    @objc private func clovesViewController() {
        self.dismiss(animated: true)
    }
    
    private func settingImageView() {
        imageView.image.image = content?.image
    }
    
    private func settingStackView() {
        stackView.axis = .vertical
        stackView.spacing = Const.Detail.spacingStackView
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(descriptionLabel)
    }
    
    private func settingTitleLabel() {
        titleLabel.text = content?.title
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont.systemFont(ofSize: Const.Font.sizeTitle)
    }
    
    private func settingDescriptionLabel() {
        descriptionLabel.text = content?.description
        descriptionLabel.textAlignment = .center
        descriptionLabel.layer.cornerRadius = Const.Detail.descriptionCornerRadius
        descriptionLabel.layer.masksToBounds = true
    }
    
    private func addingSubviews() {
        view.addSubview(imageView)
        view.addSubview(stackView)
    }
    
    private func notTranslatesAutoresizingMaskIntoConstraints() {
        stackView.translatesAutoresizingMaskIntoConstraints = false
        imageView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    private func settingConstraints() {
        
        NSLayoutConstraint.activate([
            imageView.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor),
            imageView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            
            stackView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            stackView.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: Const.Detail.stackTopOffset),
            stackView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,
                                               constant: Const.Detail.stackLeadingOffset),
            stackView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor,
                                                constant: -Const.Detail.stackTrailing),
        ])
    }
}
