//
//  Content.swift
//  Task 2
//
//  Created by Sergey Parfentchik on 12.02.22.
//
import UIKit

struct Content {
    let image: UIImage?
    let title: String
    let description: String
}

// MARK: - Public Func

func createHundredElements() -> [Content] {
    var contents: [Content] = []
    
    for item in 1 ... Const.Content.numberOfCells {
        let image = UIImage(named: Const.Content.imageName + String(item % 10))
        let title = Const.Content.title + String(item)
        let description = Const.Content.description + String(item)
        let content = Content.init(image: image, title: title, description: description)
        contents.append(content)
    }
    return contents
}

